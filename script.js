// Function and variable declarations first. Search "Scripts starts here!" commment for the action.

// First array is used to process data, second one is used to check if it is an original input (user entered input) as a reference.


var pageCache = [

    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""]

];
var pageOriginals = [

    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""],
    ["", "", "", "", "", "", "", "", ""]

];

// Contains HTML, see IIFE named storePageReset() and resetPage()below.
var pageStatus;


// Takes whatever is in the cells and stores into the arrays.

function storePageStatus() {
    for (var i = 1; i <= 9; i++) {
        for (var j = 1; j <= 9; j++) {

            if (($("#row" + i).children("#cell" + j).val())) {
                pageCache[i - 1][j - 1] = Number($("#row" + i).children("#cell" + j).val());
                pageOriginals[i - 1][j - 1] = Number($("#row" + i).children("#cell" + j).val());
            }
        }
    }
}


// After the solve, takes whatever is in the array and puts it into the HTML.

function printPage() {
    for (var i = 1; i <= 9; i++) {
        for (var j = 1; j <= 9; j++) {

            // Below code adds a class to original cells to differentiate which is original input
            // and which is not, so that CSS can assign a different background color at the end.

            if (pageOriginals[i - 1][j - 1]) {
                $("#row" + i).children("#cell" + j).addClass("original");
            } else {
                $("#row" + i).children("#cell" + j).val(pageCache[i - 1][j - 1]);
            }
        }
    }
}

// Functions that gets and sets data to the arrays. Subtract 1 because arrays start from 0 and I don't.

function getRowCell(rowNumber, cellNumber) {
    return pageCache[rowNumber - 1][cellNumber - 1];
}

function setRowCell(rowNumber, cellNumber, value) {
    pageCache[rowNumber - 1][cellNumber - 1] = value;
}


// Retreives the original HTML code of the page from variable pageStatus.

function resetPage() {
    $("#tableContainer").html(pageStatus);
}
// Resets arrays to nothing. resetPage and resetArrays are seperate functions because when user inputs
// invalid data, arrays needs to be reset and page itself does not. See solveButton() below.
function resetArrays() {
    for (var i = 1; i <= 9; i++) {
        for (var j = 1; j <= 9; j++) {
            pageCache[i - 1][j - 1] = "";
            pageOriginals[i - 1][j - 1] = "";
        }
    }
}

// Displays messages on the screen. Fades out with time.
function setMessage(yourMessage) {
    $("#messageSpan").html(yourMessage).fadeIn(600);
    function resetMessage() {
        $("#messageSpan").fadeOut(600);
    };
    setTimeout(resetMessage, 3000);
}

// Seperate checker functions for rows, columns, and blocks.

function checkRow(rowNumber, cellNumber) {
    for (var i = 1; i <= 9; i++) {
        var currentCellValue = getRowCell(rowNumber, cellNumber);
        var loopCellValue = getRowCell(rowNumber, i);
        if (cellNumber == i) {
            continue;
        } else if (loopCellValue == "") {
            continue;
        } else if (currentCellValue == loopCellValue) {
            return false;
        }
    }
    return true;
}

function checkColumn(rowNumber, cellNumber) {
    for (var i = 1; i <= 9; i++) {
        var currentCellValue = getRowCell(rowNumber, cellNumber);
        var loopRowValue = getRowCell(i, cellNumber);
        if (rowNumber == i) {
            continue;
        } else if (loopRowValue == "") {
            continue;
        } else if (currentCellValue == loopRowValue) {
            return false;
        }
    }
    return true;
}

function checkBlock(rowNumber, cellNumber) {
    var currentCellValue = getRowCell(rowNumber, cellNumber);

    function doubleChecker(rowStart, rowEnd, cellStart, cellEnd) {
        for (var i = rowStart; i <= rowEnd; i++) {
            for (var j = cellStart; j <= cellEnd; j++) {
                var loopCellValue = getRowCell(i, j)
                if ((rowNumber == i) && (cellNumber == j)) {
                    continue;
                } else if (loopCellValue == "") {
                    continue;
                } else if (currentCellValue == loopCellValue) {
                    return false;
                }
            }
        }
        return true;
    }

    // Differentiates the blocks and checks accordingly.

    if (((rowNumber >= 1) && (rowNumber <= 3)) && ((cellNumber >= 1) && (cellNumber <= 3))) {
        if (!doubleChecker(1, 3, 1, 3)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 1) && (rowNumber <= 3)) && ((cellNumber >= 4) && (cellNumber <= 6))) {
        if (!doubleChecker(1, 3, 4, 6)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 1) && (rowNumber <= 3)) && ((cellNumber >= 7) && (cellNumber <= 9))) {
        if (!doubleChecker(1, 3, 7, 9)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 4) && (rowNumber <= 6)) && ((cellNumber >= 1) && (cellNumber <= 3))) {
        if (!doubleChecker(4, 6, 1, 3)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 4) && (rowNumber <= 6)) && ((cellNumber >= 4) && (cellNumber <= 6))) {
        if (!doubleChecker(4, 6, 4, 6)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 4) && (rowNumber <= 6)) && ((cellNumber >= 7) && (cellNumber <= 9))) {
        if (!doubleChecker(4, 6, 7, 9)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 7) && (rowNumber <= 9)) && ((cellNumber >= 1) && (cellNumber <= 3))) {
        if (!doubleChecker(7, 9, 1, 3)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 7) && (rowNumber <= 9)) && ((cellNumber >= 4) && (cellNumber <= 6))) {
        if (!doubleChecker(7, 9, 4, 6)) {
            return false;
        } else {
            return true;
        }
    } else if (((rowNumber >= 7) && (rowNumber <= 9)) && ((cellNumber >= 7) && (cellNumber <= 9))) {
        if (!doubleChecker(7, 9, 7, 9)) {
            return false;
        } else {
            return true;
        }
    }
}

// Puts all the checkers into an easily accessible form.

function checkAll(rowNumber, cellNumber) {
    return (checkColumn(rowNumber, cellNumber) && checkRow(rowNumber, cellNumber)) && checkBlock(rowNumber, cellNumber);
}


// Main function that solves given table.

function solve() {
    // Local variables that store the value of the current cell, row, and its current value.
    var rowNumber = 1;

    // We start with cellNumber = 0 here and goForward() at the bottom because first cell might be an original cell.
    // goForward ensures that we are in a non-original cell to start with.

    var cellNumber = 0;
    var currentCellValue = 1;

    // Checks if the cell we are currently in is an original (user input) cell or not.

    function checkIfFull(rowNumber, cellNumber) {
        if (pageOriginals[rowNumber - 1][cellNumber - 1]) {
            return true;
        } else {
            return false;
        }
    }

    // These two functions childrens next or previous non-original cell.

    function goForward() {
        do {
            cellNumber += 1;
            if (cellNumber > 9) {
                cellNumber = 1;
                rowNumber += 1;
                if (rowNumber > 9) {
                    return false;
                }
            }
        } while (checkIfFull(rowNumber, cellNumber));
    }

    function goBack() {
        cellNumber--
        if (cellNumber < 1) {
            cellNumber = 9;
            rowNumber -= 1;

            //This should never happen, hopefully.

            if (rowNumber < 1) {
                setMessage("Something is really wrong!");
            }
        }
        if (checkIfFull(rowNumber, cellNumber)) {
            goBack();
        }
    }

    // childrens first non-original cell as commented above.

    goForward();

    // Backtracking loop that is based on the pseudo code from wikipedia (See: "https://en.wikipedia.org/wiki/Sudoku_solving_algorithms#Backtracking").
    // Summary;
    // Places currentCellValue, checks if it fits the rules. If it does, then goes next non-original cell and sets currentCellValue to 1.
    // If it doesn't, then increases the value by one and checks again. If value doesn't meet the criteria and goes above 9 then it devares
    // the current value, goes back to a previous non-original value and increases that value by one, "continues" the loop by checking it again.

    while (true) {

        if (currentCellValue > 9) {
            setRowCell(rowNumber, cellNumber, "");
            goBack();
            currentCellValue = +getRowCell(rowNumber, cellNumber) + 1;
            continue;
        }

        setRowCell(rowNumber, cellNumber, currentCellValue)

        if (checkAll(rowNumber, cellNumber)) {
            // Checking if goForward() returns false or not still executes the function.
            if (goForward() === false) {
                //If goForward() returns false that means it is finished. Print the page and break the loop!
                setMessage("Done!")
                printPage();
                break;
            } else {
                //Sets currentCellValue 1 for the next cell.
                currentCellValue = 1;
            }

        } else {
            currentCellValue += 1;
        }
    }
}

// Script starts here!

// Stores the original html code of the page

(function storePageReset() {
    pageStatus = $("#tableContainer").html();
})();

$(".solveButton").click(function solveButton() {
    storePageStatus();
    // Checks the input if it is valid and doesn't break the rules of sudoku.
    var regEx = /^$|^[1-9]$/
    for (var i = 1; i <= 9; i++) {
        for (var j = 1; j <= 9; j++) {
            if ((!(checkAll(i, j))) || !(regEx.test(getRowCell(i, j)))) {
                setMessage("Something is wrong with input, please check again!");
                resetArrays();
                return;
            }
        }
    }
    // Initialize solve function
    solve();
});

$(".resetButton").click(function resetButton() {
    resetPage();
    resetArrays();
});

$("input").keyup(function (event) {
    if (event.keyCode == 13) {
        $(".solveButton").click();
    }
});

